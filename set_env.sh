#!/bin/bash


#cmake -DCMAKE_CXX_COMPILER=/home/tools/gcc11/bin/g++ -DCMAKE_C_COMPILER=/home/tools/gcc11/bin/gcc .. /
#cmake .. -DCMAKE_CXX_COMPILER=clang++ -DCMAKE_C_COMPILER=clang #-DCMAKE_BUILD_TYPE=$BUILD_TYPE
cmake -DCMAKE_C_COMPILER=/home/tools/gcc/bin/gcc -DCMAKE_CPP_COMPILER=/home/tools/gcc/bin/g++ .. /
make

cd ..
chmod -R 775 ./build_
chmod -R 775 ./out_
chgrp -R HwHiAiUser ./build_
chgrp -R HwHiAiUser ./out_
