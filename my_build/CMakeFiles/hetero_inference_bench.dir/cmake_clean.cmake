file(REMOVE_RECURSE
  "../out_/hetero_inference_bench"
  "../out_/hetero_inference_bench.pdb"
  "CMakeFiles/hetero_inference_bench.dir/src/engine/onnx_engine.cpp.o"
  "CMakeFiles/hetero_inference_bench.dir/src/engine/onnx_engine.cpp.o.d"
  "CMakeFiles/hetero_inference_bench.dir/src/inference_runner.cpp.o"
  "CMakeFiles/hetero_inference_bench.dir/src/inference_runner.cpp.o.d"
  "CMakeFiles/hetero_inference_bench.dir/src/main.cpp.o"
  "CMakeFiles/hetero_inference_bench.dir/src/main.cpp.o.d"
  "CMakeFiles/hetero_inference_bench.dir/src/threading/concurrent_queue.cpp.o"
  "CMakeFiles/hetero_inference_bench.dir/src/threading/concurrent_queue.cpp.o.d"
  "CMakeFiles/hetero_inference_bench.dir/src/threading/concurrent_queue_atomic.cpp.o"
  "CMakeFiles/hetero_inference_bench.dir/src/threading/concurrent_queue_atomic.cpp.o.d"
  "CMakeFiles/hetero_inference_bench.dir/src/threading/rw_mutex.cpp.o"
  "CMakeFiles/hetero_inference_bench.dir/src/threading/rw_mutex.cpp.o.d"
  "CMakeFiles/hetero_inference_bench.dir/src/threading/task.cpp.o"
  "CMakeFiles/hetero_inference_bench.dir/src/threading/task.cpp.o.d"
  "CMakeFiles/hetero_inference_bench.dir/src/threading/thread_pool.cpp.o"
  "CMakeFiles/hetero_inference_bench.dir/src/threading/thread_pool.cpp.o.d"
  "CMakeFiles/hetero_inference_bench.dir/src/threading/worked_thread.cpp.o"
  "CMakeFiles/hetero_inference_bench.dir/src/threading/worked_thread.cpp.o.d"
  "CMakeFiles/hetero_inference_bench.dir/src/utils.cpp.o"
  "CMakeFiles/hetero_inference_bench.dir/src/utils.cpp.o.d"
)

# Per-language clean rules from dependency scanning.
foreach(lang CXX)
  include(CMakeFiles/hetero_inference_bench.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
