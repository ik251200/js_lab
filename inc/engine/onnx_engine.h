/*
* @file onnx_engine.h
*
* Copyright (C) Huawei Technologies Co., Ltd. 2020-2021. All rights reserved.
* Description: onnx engine
* Author: Egor Filimonov
* Create: 2021/04/21
* Notes:
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/
#ifndef _ONNX_ENGINE_H_
#define _ONNX_ENGINE_H_
#include "utils.h"
#include <string>
#include "base_engine.h"

#include <providers/dnnl/dnnl_provider_factory.h>
#include <onnxruntime_cxx_api.h>

#define ONNX_TYPE_ALLOC(onnx_type, ptr, len)                                       \
switch (onnx_type) {                                                               \
    case (ONNXTensorElementDataType::ONNX_TENSOR_ELEMENT_DATA_TYPE_FLOAT): {       \
        ptr = new float[len];                                                      \
      break;                                                                       \
    }                                                                              \
    case (ONNXTensorElementDataType::ONNX_TENSOR_ELEMENT_DATA_TYPE_UINT8): {       \
        ptr = new uint8_t[len];                                                    \
      break;                                                                       \
    }                                                                              \
    case (ONNXTensorElementDataType::ONNX_TENSOR_ELEMENT_DATA_TYPE_INT8): {        \
        ptr = new int8_t[len];                                                     \
      break;                                                                       \
    }                                                                              \
    case (ONNXTensorElementDataType::ONNX_TENSOR_ELEMENT_DATA_TYPE_INT32): {       \
        ptr = new int32_t[len];                                                    \
      break;                                                                       \
    }                                                                              \
    case (ONNXTensorElementDataType::ONNX_TENSOR_ELEMENT_DATA_TYPE_INT64): {       \
        ptr = new int64_t[len];                                                    \
      break;                                                                       \
    }                                                                              \
    default:                                                                       \
        break;                                                                     \
}

#define ONNX_GET_SIZE_TYPE(onnx_type, type_size)                                   \
switch (onnx_type) {                                                               \
    case (ONNXTensorElementDataType::ONNX_TENSOR_ELEMENT_DATA_TYPE_FLOAT): {       \
        type_size = sizeof(float);                                                 \
      break;                                                                       \
    }                                                                              \
    case (ONNXTensorElementDataType::ONNX_TENSOR_ELEMENT_DATA_TYPE_UINT8): {       \
        type_size = sizeof(uint8_t);                                               \
      break;                                                                       \
    }                                                                              \
    case (ONNXTensorElementDataType::ONNX_TENSOR_ELEMENT_DATA_TYPE_INT8): {        \
        type_size = sizeof(int8_t);                                                \
      break;                                                                       \
    }                                                                              \
    case (ONNXTensorElementDataType::ONNX_TENSOR_ELEMENT_DATA_TYPE_INT32): {       \
        type_size = sizeof(int32_t);                                               \
      break;                                                                       \
    }                                                                              \
    case (ONNXTensorElementDataType::ONNX_TENSOR_ELEMENT_DATA_TYPE_INT64): {       \
        type_size = sizeof(int64_t);                                               \
      break;                                                                       \
    }                                                                              \
    default:                                                                       \
        break;                                                                     \
}

#define ONNX_CREATE_TENSOR(onnx_type, tensor, memory_info, out, node_size, dim_data, dim_size) \
switch (onnx_type) {                                                                           \
    case (ONNXTensorElementDataType::ONNX_TENSOR_ELEMENT_DATA_TYPE_FLOAT): {                   \
        tensor.push_back(Ort::Value::CreateTensor<float>(memory_info,                          \
                static_cast<float*>(out), node_size, dim_data, dim_size));                     \
      break;                                                                                   \
    }                                                                                          \
    case (ONNXTensorElementDataType::ONNX_TENSOR_ELEMENT_DATA_TYPE_UINT8): {                   \
        tensor.push_back(Ort::Value::CreateTensor<uint8_t>(memory_info,                        \
                static_cast<uint8_t*>(out), node_size, dim_data, dim_size));                   \
      break;                                                                                   \
    }                                                                                          \
    case (ONNXTensorElementDataType::ONNX_TENSOR_ELEMENT_DATA_TYPE_INT8): {                    \
        tensor.push_back(Ort::Value::CreateTensor<int8_t>(memory_info,                         \
                static_cast<int8_t*>(out), node_size, dim_data, dim_size));                    \
      break;                                                                                   \
    }                                                                                          \
    case (ONNXTensorElementDataType::ONNX_TENSOR_ELEMENT_DATA_TYPE_INT32): {                   \
        tensor.push_back(Ort::Value::CreateTensor<int32_t>(memory_info,                        \
                static_cast<int32_t*>(out), node_size, dim_data, dim_size));                   \
      break;                                                                                   \
    }                                                                                          \
    case (ONNXTensorElementDataType::ONNX_TENSOR_ELEMENT_DATA_TYPE_INT64): {                   \
        tensor.push_back(Ort::Value::CreateTensor<int64_t>(memory_info,                        \
                static_cast<int64_t*>(out), node_size, dim_data, dim_size));                   \
      break;                                                                                   \
    }                                                                                          \
    default:                                                                       \
        break;                                                                     \
}

/**
 * OnnxEngine
 */
class OnnxEngine: public BaseEngine {
public:
    OnnxEngine(); 
    ~OnnxEngine();
    virtual Result PrintDesc();
    virtual Result InitEngine(std::string model_path);
    virtual vector<void*> GetOutputData();
    virtual Result ExecuteInference(const std::vector<std::vector<void*>> &outs);

private:
    Ort::Env env_;
    Ort::SessionOptions session_options_;
    Ort::AllocatorWithDefaultOptions allocator_;
    Ort::Session* session_;
    std::vector<std::vector<int64_t>> inputs_dims_;
    std::vector<std::vector<int64_t>> outputs_dims_;
    std::vector<const char*> input_node_names_;
    std::vector<const char*> output_node_names_;
};

#endif
