/*
* @file base_engine.h
*
* Copyright (C) Huawei Technologies Co., Ltd. 2020-2021. All rights reserved.
* Description: base engine
* Author: Egor Filimonov
* Create: 2021/04/21
* Notes:
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/
#ifndef _BASE_ENGINE_H_
#define _BASE_ENGINE_H_
#include "utils.h"
#include <string>
#include <iostream>

using namespace std;

/**
 * BaseEngine
 */
class BaseEngine {
public:
    BaseEngine()
    : num_inputs_(0)
    , num_outputs_(0)
    , input_size_(0)
    , output_size_(0)
    , inference_time_(0.0)
    , device_id_(0)
    {}

    virtual ~BaseEngine() 
    {}

    size_t GetNumInputs() const {
        return num_inputs_;
    }

    size_t GetNumOutputs() const {
        return num_outputs_;
    }

    size_t GetInputSize() const {
        return input_size_;
    }

    std::vector<size_t> GetInputNodeSizes() const {
        return input_node_sizes_;
    }

    std::vector<size_t> GetOutputNodeSizes() const {
        return output_node_sizes_;
    }

    size_t GetOutputSize() const {
        return output_size_;
    }

    size_t GetInferenceTime() const {
        return inference_time_;
    }

    double GetInferTime() const {
        return inference_time_;
    }

    void SetDeviceId(const uint32_t device_id) {
        device_id_ = device_id;
    }

    uint32_t GetDeviceId() const {
        return device_id_;
    }

    void SetId(const uint32_t id)  {
        id_ = id;
    }

    uint32_t GetId() {
        return id_;
    }

    void SetInputInfo(const Node &node) {
        input_idx_ = node.input_idx;
        input_order_ = node.input_order;
    }

    /**
    * @brief Virtual function to print description
    * @return result
    */
    virtual Result PrintDesc() = 0;

    /**
    * @brief Virtual function to init engine
    * @return result
    */
    virtual Result InitEngine(std::string model_path) = 0;

    /**
    * @brief Virtual function to get output data
    * @return vector of pointers
    */
    virtual vector<void*> GetOutputData() = 0;

    /**
    * @brief Virtual function to execute inference
    * @return result
    */
    virtual Result ExecuteInference(const vector<vector<void*>> &outs) = 0;

protected:
    size_t num_inputs_;
    size_t num_outputs_;
    size_t input_size_;
    size_t output_size_;
    std::vector<size_t> input_node_sizes_;
    std::vector<size_t> output_node_sizes_;
    double inference_time_;
    uint32_t device_id_;
    uint32_t id_;
    std::vector<void*> output_data_;
    std::vector<uint32_t> input_order_;
    std::vector<uint32_t> input_idx_;
};

#endif
