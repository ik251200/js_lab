/*
* @file utils.h
*
* Copyright (C) Huawei Technologies Co., Ltd. 2020-2021. All rights reserved.
* Description: utils
* Author: Egor Filimonov
* Create: 2021/04/21
* Notes:
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/
#ifndef _UTILS_H_
#define _UTILS_H_
#include <algorithm>
#include <cstddef>
#include <cstring>
#include <dirent.h>
#include <fstream>
#include <iostream>
#include <map>
#include <unordered_map>
#include <sstream>
#include <stdio.h>
#include <string>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <time.h>
#include <vector>

extern bool is_debug;

#define data_type float
#define type_accurucy 0.05F

#define INFO_LOG(fmt, args...) if(is_debug) { fprintf(stdout, "[INFO] " fmt "\n", ##args); }
#define DEBUG_LOG(fmt, args...) if(is_debug) { fprintf(stdout, "[DEBUG] " fmt "\n", ##args); }
#define WARN_LOG(fmt, args...) fprintf(stdout, "[WARN] " fmt "\n", ##args)
#define ERROR_LOG(fmt, args...) fprintf(stdout, "\n[ERROR] " fmt "\n", ##args)

typedef enum Result {
    SUCCESS = 0,
    FAILED = 1
} Result;

enum DeviceType {
    CPU = 0,
    ASCEND = 1,
    KUNNEHEAD = 2,
    // add new devices
    DATA = 100
};

struct Node {
    std::string model_path;
    DeviceType device_type;
    uint32_t device_id;
    std::vector<uint32_t> input_idx;
    std::vector<uint32_t> input_order;

    std::string ToString() {
        std::stringstream out;
        out << "Path: " << model_path << "\n";
        out << "Device Type: " << device_type << "\n";
        out << "Device Id: " << device_id << "\n";
        out << "Node idx: ";

        for (const auto &node: input_idx) {
            out << node << ' ';
        }

        out << "Input order: ";

        for (const auto &node: input_order) {
            out << node << ' ';
        }

        return out.str();
    }
};

/**
* Utils
*/
class Utils {
public:
    /**
    * @brief get data from file
    * @param[in] file_name: file name
    * @param[in] input_size: size of input data
    * @param[out] input_data: input data buffer
    */
    static Node ReadOneNode(std::ifstream &in);

    static std::vector<Node> ParseNodes(std::string input_file);

    static void ReadBinFile(std::string file_name, void* &input_data);

    static Result CheckAccurucy(std::vector<void*> ground_truth_output, std::vector<size_t> ground_truth_sizes, 
            std::vector<void*> test_output,  std::vector<size_t> test_sizes);

    static void SplitString(std::string& s, std::vector<std::string>& v, char c);

    static int str2num(const char* str);

    static std::string modelName(std::string& s);

    static std::string TimeLine();

    static void printCurrentTime();

    static void printHelpLetter();

    static double printDiffTime(time_t begin, time_t end);

    static double InferenceTimeAverage(double* x, int len);

    static double InferenceTimeAverageWithoutFirst(double* x, int len);

    static void ProfilerJson(bool isprof, std::map<char, std::string>& params);

    static void DumpJson(bool isdump, std::map<char, std::string>& params);
};

#endif
