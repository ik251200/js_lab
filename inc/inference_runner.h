/*
* @file inference_runner.h
*
* Copyright (C) Huawei Technologies Co., Ltd. 2020-2021. All rights reserved.
* Description: inference process
* Author: Egor Filimonov
* Create: 2021/04/21
* Notes:
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/
#ifndef _INFERENCE_RUNNER_H_
#define _INFERENCE_RUNNER_H_
#include "utils.h"
#include <stdio.h>
#include "onnx_engine.h"
#include "thread_pool.h"

// static std::string input_data_ftype = ".bin";
static std::string models_list_ftype = ".txt";
// static std::string ascend_model_ftype = ".om";
// static std::string openvino_model_ftype = ".xml";
// static std::string onnx_model_ftype = ".onnx";

/**
* InferenceRunner
*/
class InferenceRunner {
public:
    InferenceRunner();
    ~InferenceRunner();

    Result Init(std::vector<Node> nodes);
    Result AddLoop(std::vector<Node> nodes);
    Result RunPerf(std::vector<Node> nodes, size_t loop);
    Result RunTest(std::vector<Node> nodes, std::vector<Node> test_nodes);

private:
    size_t num_models_;
    std::vector<BaseEngine*> infer_engines_;
    std::vector<std::vector<void*> > outs_;
    std::vector<std::shared_ptr<Task> > tasks_list_;
    //unordered_map<int, std::shared_ptr<Task>> tasks_;
    unordered_set<Task*> tasks_idx_;
    std::shared_ptr<std::mutex> mtx_;
    std::shared_ptr<std::condition_variable> cv_;
    ThreadPool th_;
};
#endif
