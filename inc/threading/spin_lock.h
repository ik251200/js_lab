/*
* @file rw_mutex.h
*
* Copyright (C) Huawei Technologies Co., Ltd. 2020-2021. All rights reserved.
* Description: Spin lock
* Author: Mikhail Zhelezin
* Create: 2021/05/31
* Notes:
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

#ifndef _SPIN_LOCK_H_
#define _SPIN_LOCK_H_

#include <mutex> 
#include <condition_variable>
#include <atomic>
#include <thread>

class SpinLock {
public:
    void Lock() {
        while (locked_.exchange(true)) {
            std::this_thread::yield();
        }
    }

    void Unlock() {
        locked_.store(false);
    }

private:
    std::atomic<bool> locked_{false};
};

#endif