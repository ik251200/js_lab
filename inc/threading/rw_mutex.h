/*
* @file rw_mutex.h
*
* Copyright (C) Huawei Technologies Co., Ltd. 2020-2021. All rights reserved.
* Description: RW Mutex
* Author: Mikhail Zhelezin
* Create: 2021/05/17
* Notes:
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

#ifndef _RW_MUTEX_H_
#define _RW_MUTEX_H_

#include <mutex> 
#include <condition_variable>
#include <atomic>

class RWMutex {
public:
    void RLock();

    void RUnlock();

    void WLock();

    void WUnlock();

private:
    std::atomic<int> counter_{0};
};

#endif