/*
* @file task.h
*
* Copyright (C) Huawei Technologies Co., Ltd. 2020-2021. All rights reserved.
* Description: Task class
* Author: Mikhail Zhelezin
* Create: 2021/05/17
* Notes:
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

#ifndef _TASK_H_
#define _TASK_H_

#include <base_engine.h>
#include <memory>
#include <unordered_set>
#include <mutex>
#include <condition_variable>

class Task : public std::enable_shared_from_this<Task> {
public:
    Task(BaseEngine* engine, DeviceType device_type, std::unordered_set<Task*>& tasks_idx,
            std::shared_ptr<std::mutex> mtx, std::shared_ptr<std::condition_variable> cv)
    : engine_(engine)
    , device_type_(device_type)
    , tasks_idx_(tasks_idx)
    , mtx_(mtx)
    , cv_(cv)
    {
    }

    void AddDependency(std::shared_ptr<Task> dep);

    void AddDestination(std::shared_ptr<Task> dst);

    void Run(vector<vector<void*>>& outs);

    void DeleteDependency(std::shared_ptr<Task> dep);

    bool Check();

    DeviceType GetDeviceType();

    std::vector<std::shared_ptr<Task>> NotifyDestinations();

private:
    BaseEngine* engine_;
    DeviceType device_type_;
    std::unordered_set<Task*>& tasks_idx_;
    std::shared_ptr<std::mutex> mtx_;
    std::shared_ptr<std::condition_variable> cv_;
    std::unordered_set<Task*> deps_;
    std::unordered_set<Task*> dsts_;
};

#endif