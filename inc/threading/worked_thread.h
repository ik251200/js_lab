/*
* @file worked_thread.h
*
* Copyright (C) Huawei Technologies Co., Ltd. 2020-2021. All rights reserved.
* Description: Worked Thread
* Author: Mikhail Zhelezin
* Create: 2021/05/17
* Notes:
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/
#ifndef _WORKED_THREAD_H_
#define _WORKED_THREAD_H_

#include <memory>
#include <vector>
#include <concurrent_queue_atomic.h>
#include <thread>

class WorkerThread : public std::enable_shared_from_this<WorkerThread> {
public:
    WorkerThread(std::vector<std::shared_ptr<ConcurrentQueueAtomic>> &queues, std::vector<std::shared_ptr<mutex>> &mutexes,
                 std::vector<std::shared_ptr<condition_variable>> &cvs, DeviceType device_type) :
            queues_(queues),  mutexes_(mutexes), cvs_(cvs), device_type_(device_type) {
    }
    
    void Execute(std::vector<std::vector<void*>>& outs);

    void Stop();

    ~WorkerThread() = default;

protected:
    std::vector<std::shared_ptr<ConcurrentQueueAtomic>> &queues_;
    std::vector<std::shared_ptr<mutex>> &mutexes_;
    std::vector<std::shared_ptr<condition_variable>> &cvs_;
    DeviceType device_type_;
    std::thread thread_;
    bool end_ = false;
};

#endif