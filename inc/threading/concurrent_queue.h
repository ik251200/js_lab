/*
* @file concurrent_queue.h
*
* Copyright (C) Huawei Technologies Co., Ltd. 2020-2021. All rights reserved.
* Description: Concurrent Queue
* Author: Mikhail Zhelezin
* Create: 2021/05/17
* Notes:
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

#ifndef _CONCURRENT_QUEUE_H_
#define _CONCURRENT_QUEUE_H_

#include <queue>
#include <rw_mutex.h>
#include <task.h>

class ConcurrentQueue {
public:
    size_t Size();

    bool Empty();

    std::shared_ptr<Task> Pop();

    void Push(std::shared_ptr<Task> task);

private:
    std::queue<std::shared_ptr<Task>> queue_;
    RWMutex mutex_;
};

#endif