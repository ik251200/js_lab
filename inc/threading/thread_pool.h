/*
* @file thread_pool.h
*
* Copyright (C) Huawei Technologies Co., Ltd. 2020-2021. All rights reserved.
* Description: Thread Pool
* Author: Mikhail Zhelezin
* Create: 2021/05/17
* Notes:
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/
#ifndef _THREAD_POOL_H_
#define _THREAD_POOL_H_

#include <base_engine.h>
#include <queue>
#include <thread>
#include <mutex> 
#include <condition_variable>
#include <assert.h>
#include <memory>
#include <unordered_set>
#include <rw_mutex.h>
#include <task.h>
#include <worked_thread.h>
#include <concurrent_queue_atomic.h>

class ThreadPool { 
public:
    ThreadPool(size_t devices, vector<size_t> devices_count);

    void AddTask(std::shared_ptr<Task> task, DeviceType device_type);

    void Start(vector<vector<void*>>& outs);

    void Stop();

private:
    std::vector<std::shared_ptr<WorkerThread>> worked_threads_;
    std::vector<std::shared_ptr<mutex>> mutexes_;
    std::vector<std::shared_ptr<condition_variable>> cvs_;
    std::vector<std::shared_ptr<ConcurrentQueueAtomic>> queues_;
};

#endif