#!/bin/bash

export MODEL_LIST=$1

if [ "$2" ]; then
  export DEBUG_FLAG=$2
else
  export DEBUG_FLAG=false
fi
if [ "$3" ]; then
  export ITERATION_NUMBER=$3
else
  export ITERATION_NUMBER=1
fi
if [ "$4" ]; then
  export THREADS_NUMBER=$4
else
  export THREADS_NUMBER=32
fi
if [ "$5" ]; then
  export USE_OPENVINO=$5
else
  export USE_OPENVINO=false
fi
if [ "$6" ]; then
  export TEST_MODEL=$6
else
  export TEST_MODEL="NONE"
fi
source /home/tools/set_env_clang.sh
source /home/tools/set_env_gcc.sh
source /home/tools/set_env_kml.sh

export LD_LIBRARY_PATH=/usr/local/Ascend/ascend-toolkit/latest/acllib/lib64/:$LD_LIBRARY_PATH

export OMP_NUM_THREADS=$THREADS_NUMBER

export OMP_MAX_ACTIVE_LEVELS=1
export OMP_SCHEDULE="static,1"


./out_/hetero_inference_bench        \
    --models-list $MODEL_LIST        \
    --debug $DEBUG_FLAG              \
    --loop $ITERATION_NUMBER         \
    --threads-number $THREADS_NUMBER \
    --use-openvino $USE_OPENVINO     \
    --test-model $TEST_MODEL

