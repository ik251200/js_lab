/*
* @file onnx_engine.cpp
*
* Copyright (C) Huawei Technologies Co., Ltd. 2020-2021. All rights reserved.
* Description: onnx engine
* Author: Egor Filimonov
* Create: 2021/04/21
* Notes:
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/
#include "onnx_engine.h"
#include "utils.h"
#include <cstddef>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>

using namespace std;
extern bool is_debug;
extern int threads_number;
extern bool use_openvino;

OnnxEngine::OnnxEngine() {}

OnnxEngine::~OnnxEngine() {
    for(size_t i = 0; i < num_outputs_; i++) {
        free(output_data_[i]);
    }
}

Result OnnxEngine::InitEngine(std::string model_path)
{
    Result ret = SUCCESS;

    OrtOpenVINOProviderOptions options;

    if(use_openvino) {
        options.device_type = "CPU_FP32";
        options.enable_vpu_fast_compile = 0;
        options.device_id = "";
        options.num_of_threads = threads_number;
        options.use_compiled_network = false;
        options.blob_dump_path = "";

        session_options_.AppendExecutionProvider_OpenVINO(options);
    } 
    // else {
    //     session_options_.SetIntraOpNumThreads(threads_number);
    // }

    session_ = new Ort::Session(env_, model_path.c_str(), session_options_);

    num_inputs_ = session_->GetInputCount();
    num_outputs_ = session_->GetOutputCount();

    input_size_ = 1;

    for (size_t i = 0; i < num_inputs_; i++) {

        char* input_name = session_->GetInputName(i, allocator_);
        
        input_node_names_.push_back(input_name);

        Ort::TypeInfo input_type_info = session_->GetInputTypeInfo(i);

        std::vector<int64_t> input_node_dims = input_type_info.GetTensorTypeAndShapeInfo().GetShape();

        size_t node_size = 1;

        for (size_t j = 0; j < input_node_dims.size(); j++) {
            node_size *= input_node_dims[j];
        }

        input_size_ *= node_size;
        input_node_sizes_.push_back(node_size);
        inputs_dims_.push_back(input_node_dims);
    }

    output_size_ = 1;

    output_data_ = vector<void*>(num_outputs_);

    for(size_t i = 0; i < num_outputs_; i++) {
        char* output_name = session_->GetOutputName(i, allocator_);

        output_node_names_.push_back(output_name);

        Ort::TypeInfo output_type_info = session_->GetOutputTypeInfo(i);

        std::vector<int64_t> output_node_dims = output_type_info.GetTensorTypeAndShapeInfo().GetShape();

        size_t node_size = 1;

        for (size_t j = 0; j < output_node_dims.size(); j++) {
            node_size *= output_node_dims[j];
        }

        output_size_ *= node_size;
        output_node_sizes_.push_back(node_size);
        outputs_dims_.push_back(output_node_dims);

        auto onnx_type = output_type_info.GetTensorTypeAndShapeInfo().GetElementType();
        ONNX_TYPE_ALLOC(onnx_type, output_data_[i], node_size) // new data_type[node_size];
    }

    if (is_debug) {
        ret = PrintDesc();

        if (ret != SUCCESS) {
            ERROR_LOG("ONNX Print model descrtption failed");
            return FAILED;
        }
    }

    return ret;
}

Result OnnxEngine::PrintDesc()
{
    INFO_LOG("ONNX Start print model description");
    INFO_LOG("ONNX Number of inputs = %zu:", num_inputs_);

    for (size_t i = 0; i < num_inputs_; i++) {        
        INFO_LOG("ONNX Input %zu : name=%s num_dims = %zu", i, input_node_names_[i], inputs_dims_[i].size());
        INFO_LOG("ONNX The dims of %zu input:", i);

        for (auto dim: inputs_dims_[i]) {
            printf("%zu ", dim);
        }

        printf("\n");
    }

    INFO_LOG("ONNX Number of outputs = %zu:", num_outputs_);

    for(size_t i = 0; i < num_outputs_; i++) {
        INFO_LOG("ONNX Output %zu : name=%s num_dims = %zu", i, output_node_names_[i], outputs_dims_[i].size());
        INFO_LOG("ONNX The dims of %zu output:", i);

        for (auto dim: outputs_dims_[i]) {
            printf("%zu ", dim);
        }

        printf("\n");
    }

    INFO_LOG("ONNX End print model description");

    return SUCCESS;
}

vector<void*> OnnxEngine::GetOutputData() {
    return output_data_;
}

Result OnnxEngine::ExecuteInference(const vector<vector<void*>> &outs) {
    struct timeval t1, t2;
    gettimeofday(&t1, NULL);

    std::vector<Ort::Value> batch_input_tensors;
    auto memory_info = Ort::MemoryInfo::CreateCpu(OrtArenaAllocator, OrtMemTypeDefault);

    for (size_t i = 0; i < num_inputs_; i++) {
        Ort::TypeInfo input_type_info = session_->GetInputTypeInfo(i);
        auto onnx_type = input_type_info.GetTensorTypeAndShapeInfo().GetElementType();
        ONNX_CREATE_TENSOR(onnx_type, batch_input_tensors, memory_info, outs[input_idx_[i]][input_order_[i]],
                input_node_sizes_[i], inputs_dims_[i].data(), inputs_dims_[i].size())
    }

    auto onnx_output = session_->Run(Ort::RunOptions{nullptr}, input_node_names_.data(), batch_input_tensors.data(),
                            input_node_names_.size(), output_node_names_.data(), output_node_names_.size());

    for (size_t i = 0; i < num_outputs_; i++) {
        const void* out_data = onnx_output[i].GetTensorMutableData<void>();

        Ort::TypeInfo output_type_info = session_->GetOutputTypeInfo(i);
        auto onnx_type = output_type_info.GetTensorTypeAndShapeInfo().GetElementType();
        size_t type_size = 0;
        ONNX_GET_SIZE_TYPE(onnx_type, type_size)
        size_t len = output_node_sizes_[i] * type_size;

        memcpy(output_data_[i], out_data, len);
    }

    gettimeofday(&t2, NULL);
    inference_time_ += 1000 * (t2.tv_sec - t1.tv_sec) + (t2.tv_usec - t1.tv_usec) / 1000.0;    
    DEBUG_LOG("ONNX Inference time, ms: %.3f", inference_time_);

    DEBUG_LOG("ONNX Model execute success");

    return SUCCESS;
}
