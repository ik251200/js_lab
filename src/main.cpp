/*
* @file main.cpp
*
* Copyright (C) Huawei Technologies Co., Ltd. 2020-2021. All rights reserved.
* Description: main
* Author: Egor Filimonov
* Create: 2021/04/21
* Notes:
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

#include "inference_runner.h"
#include "utils.h"
#include <getopt.h>
#include <thread> 
#include <chrono> 

using namespace std;

int threads_number = 1;
bool is_debug = false;
bool use_openvino = false;

void InitAndCheckParams(int argc, char* argv[], map<char, string>& params) {
    const char* optstring = "i::g::l::n::o::t::";
    int c, index;

    struct option opts[] = { 
        { "models-list", required_argument, NULL, 'i' },
        { "debug", required_argument, NULL, 'g' },
        { "loop", required_argument, NULL, 'l' },
        { "threads-number", required_argument, NULL, 'n' },
        { "use-openvino", required_argument, NULL, 'o' },
        { "test-model", required_argument, NULL, 't' },
        { "help", no_argument, NULL, 1 },
        { 0, 0, 0, 0 } };

    while ((c = getopt_long(argc, argv, optstring, opts, &index)) != -1) {
        string check;
        switch (c) {
        case 'i':
            check = optarg;
            if (check.find(models_list_ftype) != string::npos) {
                params['i'] = optarg;
                break;
            } else {
                ERROR_LOG("input models description file type for should be .txt!");
                exit(0);
            }
        case 't':
            check = optarg;
            if (check.find(models_list_ftype) != string::npos) {
                params['t'] = optarg;
                break;
            } else {
                params['t'] = "";
                INFO_LOG("no model for accurucy test!");
                break;
            }
        case 'o':
            params['o'] = optarg;
            break;
        case '?':
            ERROR_LOG("Unknown paramenter.");
            ERROR_LOG("Execute sample failed!");
            Utils::printHelpLetter();
            exit(0);
        case 'l':
            params['l'] = optarg;
            break;
        case 'n':
            threads_number = Utils::str2num(optarg);
            if (threads_number > 1000 || threads_number < 1) {
                printf("threads_number must in 1 to 1000\n");
                exit(0);
            }
			break;
        case 'g':
            params['g'] = optarg;
            break;
        case 1:
            Utils::printHelpLetter();
            exit(0);
        default:
            ERROR_LOG("Unknown paramenter.");
            ERROR_LOG("Execute sample failed!");
            Utils::printHelpLetter();
            exit(0);
        }
    }
}

int main(int argc, char* argv[]) {
    map<char, string> params;

    InitAndCheckParams(argc, argv, params);

    if (params['g'].compare("true") == 0) {
        is_debug = true;
    }

    INFO_LOG("******************************");
    INFO_LOG("Test Start!");

    if (params.empty()) {
        ERROR_LOG("Invalid params.");
        ERROR_LOG("Execute sample failed.");
        Utils::printHelpLetter();
        return FAILED;
    }

    if (params['o'].compare("true") == 0) {
        use_openvino = true;
    }

    vector<Node> nodes = Utils::ParseNodes(params['i']);
    InferenceRunner inference_runner;

    size_t loop = Utils::str2num(params['l'].c_str());

    Result ret = inference_runner.RunPerf(nodes, loop);
    if (ret != SUCCESS) {
        ERROR_LOG("Run performance inference failed.");
        return FAILED;
    }
            
    
    if (params['t'].find(models_list_ftype) != string::npos) {
        INFO_LOG("Run accurucy test");
        vector<Node> ground_truth_nodes = Utils::ParseNodes(params['t']);
        ret = inference_runner.RunTest(nodes, ground_truth_nodes);

        if (ret != SUCCESS) {
            ERROR_LOG("Accurucy test failed.");
            return FAILED;
        } else {
            INFO_LOG("Accurucy test success.");
        }
    } else {
        INFO_LOG("There is no model for accurucy test");
    }

    INFO_LOG("Execute sample success.");
    INFO_LOG("Test Finish!");
    INFO_LOG("******************************");

    return SUCCESS;
}
