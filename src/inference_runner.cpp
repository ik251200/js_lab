/*
* @file inference_runner.cpp
*
* Copyright (C) Huawei Technologies Co., Ltd. 2020-2021. All rights reserved.
* Description: inference process
* Author: Egor Filimonov
* Create: 2021/04/21
* Notes:
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/
#include "inference_runner.h"
#include <thread>
//#include <tbb/tbb.h>

using namespace std;

extern bool is_debug;
extern bool threads_number;

InferenceRunner::InferenceRunner(): th_(3, {1, 1, 1})
{
    // ACL init

    // const char* aclConfigPath = "acl.json";
    // ret = aclInit(aclConfigPath);

}

InferenceRunner::~InferenceRunner()
{
}

Result InferenceRunner::Init(vector<Node> nodes)
{
    Result ret = SUCCESS;
    unordered_map<int, std::shared_ptr<Task>> tasks_;
    num_models_ = nodes.size();
    tasks_list_.clear();
    infer_engines_ = std::vector<BaseEngine*>(num_models_);
    outs_ = std::vector<std::vector<void*> >(num_models_);

    mtx_ = std::make_shared<std::mutex>();
    cv_ = std::make_shared<std::condition_variable>();

    for (size_t i = 0; i < num_models_; i++) {
        if (is_debug) {
            cout << "Node " << i << " :\n" << nodes[i].ToString() << endl;
        }

        switch (nodes[i].device_type)
        {
        case DeviceType::CPU:
            infer_engines_[i] = new OnnxEngine;
            break;
        case DeviceType::DATA:
            outs_[i].push_back(nullptr);
            Utils::ReadBinFile(nodes[i].model_path, outs_[i].front());
            break;
        default:
            break;
        }

        if (DeviceType::DATA == nodes[i].device_type) {
            continue;
        }

        ret = infer_engines_[i]->InitEngine(nodes[i].model_path);
        outs_[i] = infer_engines_[i]->GetOutputData();
        infer_engines_[i]->SetInputInfo(nodes[i]);
        tasks_[i] = std::make_shared<Task>(infer_engines_[i], nodes[i].device_type, tasks_idx_, mtx_, cv_);
        
        for (auto &deps: nodes[i].input_idx) {
            if (nodes[deps].device_type != DeviceType::DATA)
                tasks_[i]->AddDependency(tasks_[deps]);
        }
        if (tasks_[i]->Check()) {
            th_.AddTask(tasks_[i], nodes[i].device_type);
        }
        tasks_idx_.insert(tasks_[i].get());
        if (ret != SUCCESS) {
            ERROR_LOG("Init engine failed");
            return FAILED;
        }
    }
    for (auto &x: tasks_) {
        tasks_list_.push_back(x.second);
    }

    return SUCCESS;
}

Result InferenceRunner::AddLoop(vector<Node> nodes) 
{
    unordered_map<int, std::shared_ptr<Task>> tasks_;
    for (size_t i = 0; i < num_models_; i++) {
        if (DeviceType::DATA == nodes[i].device_type) {
            continue;
        }
        tasks_[i] = std::make_shared<Task>(infer_engines_[i], nodes[i].device_type, tasks_idx_, mtx_, cv_);
            
        for (auto &deps: nodes[i].input_idx) {
            if (nodes[deps].device_type != DeviceType::DATA)
                tasks_[i]->AddDependency(tasks_[deps]);
        }
        if (tasks_[i]->Check()) {
            th_.AddTask(tasks_[i], nodes[i].device_type);
        }
        tasks_idx_.insert(tasks_[i].get());
    }
    for (auto &x: tasks_) {
        tasks_list_.push_back(x.second);
    }

    return SUCCESS;
}

Result InferenceRunner::RunPerf(vector<Node> nodes, size_t loop)
{
    Result ret = SUCCESS;
    struct timeval t1, t2;
    
    Init(nodes);

    // Warm run
    for(size_t i = 0; i < 2; i++){
        for(size_t j = 0; j < num_models_; j++) {
            if (nodes[j].device_type != DeviceType::DATA){
                infer_engines_[j]->ExecuteInference(outs_);
            }
        }
    }

    for(size_t i = 0; i < loop - 1; i++) {
        AddLoop(nodes);
    }

    gettimeofday(&t1, NULL);

	th_.Start(outs_);
    while (!tasks_idx_.empty()) {
        std::unique_lock<std::mutex> lock(*mtx_);
        cv_->wait(lock, [&]() {return tasks_idx_.empty();});
	}
	th_.Stop();

    gettimeofday(&t2, NULL);
    double inference_time = 1000.0 * static_cast<double>(t2.tv_sec - t1.tv_sec) + static_cast<double>(t2.tv_usec - t1.tv_usec) / 1000.0;
    double average_time = inference_time / static_cast<double>(loop);
    INFO_LOG("Total Inference time, ms: %.3f", inference_time);
    INFO_LOG("Average Inference time, ms: %.3f", average_time);

    cout << average_time;

    return ret;
}

Result InferenceRunner::RunTest(vector<Node> test_nodes, vector<Node> ground_truth_nodes)
{
    Result ret = SUCCESS;

    INFO_LOG("Start accurucy testing");

    std::vector<void*> test_output = infer_engines_[num_models_ - 1]->GetOutputData();
    std::vector<size_t> test_sizes = infer_engines_[num_models_ - 1]->GetOutputNodeSizes(); 

    Init(ground_truth_nodes);
    for(size_t j = 0; j < num_models_; j++) {
        if (ground_truth_nodes[j].device_type != DeviceType::DATA) {
            infer_engines_[j]->ExecuteInference(outs_);
        }
    }

    std::vector<void*> ground_truth_output = infer_engines_[num_models_ - 1]->GetOutputData();
    std::vector<size_t> ground_truth_sizes = infer_engines_[num_models_ - 1]->GetOutputNodeSizes(); 

    ret = Utils::CheckAccurucy(ground_truth_output, ground_truth_sizes, test_output, test_sizes);

    return ret;
}
