/*
* @file concurrent_queue.cpp
*
* Copyright (C) Huawei Technologies Co., Ltd. 2020-2021. All rights reserved.
* Description: Concurrent Queue
* Author: Mikhail Zhelezin
* Create: 2021/05/17
* Notes:
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

#include <concurrent_queue.h>

size_t ConcurrentQueue::Size() {
    mutex_.RLock();
    size_t out = queue_.size();
    mutex_.RUnlock();
    return out;
}

bool ConcurrentQueue::Empty() {
    mutex_.RLock();
    bool out = queue_.empty();
    mutex_.RUnlock();
    return out;
}

std::shared_ptr<Task> ConcurrentQueue::Pop() {
    if (Empty()) return nullptr;
    mutex_.WLock();
    std::shared_ptr<Task> out = queue_.front();
    queue_.pop();
    mutex_.WUnlock();
    return out;
}    

void ConcurrentQueue::Push(std::shared_ptr<Task> task) {
    mutex_.WLock();
    queue_.push(task);
    mutex_.WUnlock();
}