/*
* @file thread_pool.cpp
*
* Copyright (C) Huawei Technologies Co., Ltd. 2020-2021. All rights reserved.
* Description: Thread Pool
* Author: Mikhail Zhelezin
* Create: 2021/05/17
* Notes:
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

#include <thread_pool.h>

ThreadPool::ThreadPool(size_t devices, vector<size_t> devices_count) {
    assert(devices == devices_count.size()); 
    for (size_t i = 0; i < devices; ++i) {
        mutexes_.push_back(std::make_shared<std::mutex>());
        cvs_.push_back(std::make_shared<std::condition_variable>());
        queues_.push_back(std::make_shared<ConcurrentQueueAtomic>());
    }
    for (size_t i = 0; i < devices; ++i) {
        for (size_t j = 0; j < devices_count[i]; ++j) {
            worked_threads_.push_back(std::make_shared<WorkerThread>(queues_, mutexes_, cvs_, DeviceType(i)));
        }
    }
}

void ThreadPool::AddTask(std::shared_ptr<Task> task, DeviceType device_type) {
    queues_[device_type]->Push(task);
}

void ThreadPool::Start(std::vector<std::vector<void*>>& outs) { 
    for (const auto& thread: worked_threads_) {
        thread->Execute(outs);
    }
}

void ThreadPool::Stop() {
    for (const auto& thread: worked_threads_) {
        thread->Stop();
    }
}