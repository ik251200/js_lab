/*
* @file rw_mutex.cpp
*
* Copyright (C) Huawei Technologies Co., Ltd. 2020-2021. All rights reserved.
* Description: RW Mutex
* Author: Mikhail Zhelezin
* Create: 2021/05/17
* Notes:
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

#include <rw_mutex.h>

void RWMutex::RLock() {
    while (true) {
        int x = counter_.load();
        if (x < 0) {
            continue;
        }
        if (counter_.compare_exchange_weak(x, x + 1)) {
            break;
        }
    }

}

void RWMutex::RUnlock() {
    while (true) {
        int x = counter_.load();
        if (counter_.compare_exchange_weak(x, x - 1)) {
            break;
        }
    }
}


void RWMutex::WLock() {
    while (true) {
        int x = counter_.load();
        if (x != 0) {
            continue;
        }
        if (counter_.compare_exchange_weak(x, -1)) {
            break;
        }
    }

}

void RWMutex::WUnlock() {
    while (true) {
        int x = counter_.load();
        if (counter_.compare_exchange_weak(x, 0)) {
            break;
        }
    }
}