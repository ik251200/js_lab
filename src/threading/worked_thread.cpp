/*
* @file worked_thread.cpp
*
* Copyright (C) Huawei Technologies Co., Ltd. 2020-2021. All rights reserved.
* Description: Worked Thread
* Author: Mikhail Zhelezin
* Create: 2021/05/17
* Notes:
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

#include <worked_thread.h>

void WorkerThread::Execute(std::vector<std::vector<void*>>& outs) {
    end_ = false;
    thread_ = std::thread([&]() {
        while (!end_) {
            std::unique_lock<std::mutex> lock(*(mutexes_[device_type_]));
            cvs_[device_type_]->wait(lock, [&]() {
                return !queues_[device_type_]->Empty() || end_;
            });
            if (end_) {
                break;
            }
            auto task = queues_[device_type_]->Pop();
            lock.unlock();
            task->Run(outs);
            auto rdy = task->NotifyDestinations();
            for (auto &rdy_task : rdy) {
                std::unique_lock<std::mutex> lock(*(mutexes_[rdy_task->GetDeviceType()]));
                queues_[rdy_task->GetDeviceType()]->Push(rdy_task);
                cvs_[rdy_task->GetDeviceType()]->notify_one();
            }
        }
    });
}

void WorkerThread::Stop() {
    end_ = true;
    cvs_[device_type_]->notify_one();
    thread_.join();
}