/*
* @file concurrent_queue.cpp
*
* Copyright (C) Huawei Technologies Co., Ltd. 2020-2021. All rights reserved.
* Description: Concurrent Queue Atomic
* Author: Mikhail Zhelezin
* Create: 2021/05/31
* Notes:
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/


#include <concurrent_queue_atomic.h>

size_t ConcurrentQueueAtomic::Size() {
    return size_.load();
}

bool ConcurrentQueueAtomic::Empty() {
    return Size() == 0;
}

std::shared_ptr<Task> ConcurrentQueueAtomic::Pop() {
    if (Empty()) return nullptr;
    tail_->lock();
    auto was = tail_;
    auto task = tail_->task;
    tail_ = tail_->next;
    size_.fetch_add(-1);
    was->unlock();
    return task;
}    

void ConcurrentQueueAtomic::Push(std::shared_ptr<Task> task) {
    if (tail_ == nullptr) {
        head_ = std::make_shared<Node>();
        head_->lock();
        head_->task = task;
        tail_ = head_;
        size_.fetch_add(1);
        head_->unlock();
        return;
    }
    head_->lock();
    head_->next = std::make_shared<Node>();
    head_->next->task = task;
    size_.fetch_add(1);
    auto was = head_;
    head_ = head_->next;
    was->unlock();
}