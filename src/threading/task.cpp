/*
* @file task.cpp
*
* Copyright (C) Huawei Technologies Co., Ltd. 2020-2021. All rights reserved.
* Description: Task class
* Author: Mikhail Zhelezin
* Create: 2021/05/17
* Notes:
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/

#include <task.h>

void Task::AddDependency(std::shared_ptr<Task> dep) {
    dep->dsts_.insert(this);
    deps_.insert(dep.get());
}

void Task::AddDestination(std::shared_ptr<Task> dst) {
        dsts_.insert(dst.get());
    }

void Task::Run(vector<vector<void*>>& outs) {
    engine_->ExecuteInference(outs);
    std::unique_lock<std::mutex> lock(*mtx_);
    tasks_idx_.erase(this);
    cv_->notify_one();
}

void Task::DeleteDependency(std::shared_ptr<Task> dep) {
    deps_.erase(dep.get());
}

bool Task::Check() {
    return deps_.empty();    
}

DeviceType Task::GetDeviceType() {
    return device_type_;
}

std::vector<std::shared_ptr<Task>> Task::NotifyDestinations() {
    std::vector<std::shared_ptr<Task>> ready;
    for (auto &x : dsts_) {
        x->deps_.erase(this);
        if (x->Check()) {
            ready.push_back(x->shared_from_this());
        }
    }
    dsts_.clear();
    return ready;
}