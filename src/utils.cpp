/*
* @file utils.cpp
*
* Copyright (C) Huawei Technologies Co., Ltd. 2020-2021. All rights reserved.
* Description: utils
* Author: Egor Filimonov
* Create: 2021/04/21
* Notes:
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
*/
#include "utils.h"
#include <sys/time.h>
#include <math.h>
#include <unordered_map>

using namespace std;

bool CompareValues(data_type a, data_type b)
{
    if(fabs(a - b) < type_accurucy) {
        return true;
    } else {
        return false;
    }
}

Node Utils::ReadOneNode (ifstream &in) {
    Node node;
    getline(in, node.model_path);
    int device_type;
    in >> device_type;
    node.device_type = DeviceType(device_type);
    in >> node.device_id;
    string input;
    in.get();
    getline(in, input);
    stringstream ss1(input);
    int number;

    while (ss1 >> number) {
        node.input_idx.push_back(number);
    }

    getline(in, input);
    stringstream ss2(input);

    while (ss2 >> number) {
        node.input_order.push_back(number);
    }

    return node;
}

vector<Node> Utils::ParseNodes(string input_file) {
    vector<Node> nodes;
    ifstream in(input_file);

    while (in.peek() != EOF) {
        nodes.push_back(ReadOneNode(in));
    }

    return nodes;
}

void Utils::ReadBinFile(string file_name, void* &data_buffer)
{
    ifstream bin_file(file_name, ifstream::binary);

    if (bin_file.is_open() == false) {
        ERROR_LOG("Open file %s failed", file_name.c_str());
        return;
    }

    bin_file.seekg(0, bin_file.end);
    uint32_t bin_file_len = bin_file.tellg();

    if (bin_file_len == 0) {
        ERROR_LOG("Binfile is empty, filename is %s", file_name.c_str());
        bin_file.close();
        return;
    }
    
    data_buffer = malloc(bin_file_len * sizeof(data_type));
    bin_file.seekg(0, bin_file.beg);
    bin_file.read(static_cast<char*>(data_buffer), bin_file_len);
    bin_file.close();
}

Result Utils::CheckAccurucy(std::vector<void*> ground_truth_output, std::vector<size_t> ground_truth_sizes, 
            std::vector<void*> test_output,  std::vector<size_t> test_sizes)
{
    Result ret = SUCCESS;
    size_t total_count = 0;
    size_t error_count = 0;


    if(ground_truth_output.size() != test_output.size()) {
        ERROR_LOG("Size of ground truth output = %zu size of heterogeneous output = %zu", 
                ground_truth_output.size(), test_output.size());
        return FAILED;
    }

    if(ground_truth_sizes.size() != test_sizes.size()) {
        ERROR_LOG("Size of ground truth output = %zu size of heterogeneous output = %zu", 
                ground_truth_sizes.size(), test_sizes.size());
        return FAILED;
    }

    for(size_t i = 0; i < test_output.size(); i++) {
        float* ground_truth_float = static_cast <float*>(ground_truth_output[i]);
        float* test_float = static_cast <float*>(test_output[i]);

        for(size_t j = 0; j < test_sizes[i]; j++) {
            if (!CompareValues(ground_truth_float[j], test_float[j])) {
                DEBUG_LOG("[%zu][%zu] element is not equal, ground truth value = %f test output = %f", 
                        i, j, ground_truth_float[j], test_float[j]);

                error_count++;
                ret = FAILED;
            }

            total_count++;
        }
    }

    if(ret == FAILED) {
        ERROR_LOG("Compare values check failed!");
        ERROR_LOG("Number of errors = %zu from = %zu - %.3f%%",
                error_count, total_count, 100.F * (float)(error_count) / (float)(total_count));
    }

    return ret;
}

void Utils::SplitString(string& s, vector<string>& v, char c) {

    string::size_type pos1, pos2;
    pos2 = s.find(c);
    pos1 = 0;

    while (string::npos != pos2) {
        string s1 = s.substr(pos1, pos2 - pos1);
        size_t n = s1.find_last_not_of(" \r\n\t");

        if (n != string::npos) {
            s1.erase(n + 1, s.size() - n);
        }

        n = s1.find_first_not_of(" \r\n\t");

        if (n != string::npos) {
            s1.erase(0, n);
        }

        v.push_back(s1);
        pos1 = pos2 + 1;
        pos2 = s.find(c, pos1);
    }

    if (pos1 != s.length()) {
        string s1 = s.substr(pos1);
        size_t n = s1.find_last_not_of(" \r\n\t");

        if (n != string::npos) {
            s1.erase(n + 1, s.size() - n);

        }

        n = s1.find_first_not_of(" \r\n\t");

        if (n != string::npos) {
            s1.erase(0, n);
        }

        v.push_back(s1);
    }
}

int Utils::str2num(const char* str) {

    int n = 0;
    int flag = 0;
    size_t i = 0;

    while (str[i] >= '0' && str[i] <= '9') {
        n = n * 10 + (str[i] - '0');
        i++;
    }

    if (flag == 1) {
        n = -n;
    }

    return n;
}

string Utils::modelName(string& s)
{
    string::size_type position1, position2;
    position1 = s.find_last_of("/");

    if (position1 == s.npos) {
        position1 = 0;
    }

    position2 = s.find_last_of(".");
    string modelName = s.substr(position1, position2 - position1);

    return modelName;
}

string Utils::TimeLine()
{
    time_t currentTime = time(NULL);
    char chCurrentTime[64];
    strftime(chCurrentTime, sizeof(chCurrentTime), "%Y%m%d_%H%M%S", localtime(&currentTime));
    string stCurrentTime = chCurrentTime;

    return stCurrentTime;
}

void Utils::printHelpLetter() {

    cout << endl;
    cout << "Usage:" << endl;
    cout << "Generate offline model inference output file example:" << endl;
    cout << "./hetero_inference_bench --models-list /home/HwHiAiUser/models.txt --input /home/HwHiAiUser/input.bin --loop 2" << endl
         << endl;

    cout << "arguments explain:" << endl;
    cout << "  --models-list    Model list config file path" << endl;
    cout << "  --input	        Input data path (only accept binary data file) 	If there are several file, please seprate by ','" << endl;
    cout << "  --loop 	        Loop time (must be in 1 to 1000)" << endl;
    cout << "  --device         Designated the device ID (must be in 0 to 255)" << endl;
    cout << "  --debug          Debug switch, print model information (true or false)" << endl;
    cout << "  --use-openvino   Flag to choose OpenVINO execution provider for Onnx Runtime (true or false)" << endl;
    cout << "  --test-model     Test model file path" << endl
         << endl
         << endl;
}

double Utils::printDiffTime(time_t begin, time_t end) {

    double diffT = difftime(begin, end);
    printf("The inference time is: %f millisecond\n", 1000 * diffT);

    return diffT * 1000;
}

double Utils::InferenceTimeAverage(double* x, int len) {

    double sum = 0;

    for (int i = 0; i < len; i++) {
        sum += x[i];
    }

    return sum / len;
}

double Utils::InferenceTimeAverageWithoutFirst(double* x, int len) {

    double sum = 0;

    for (int i = 0; i < len; i++) {
        if (i != 0) {
            sum += x[i];
        }
    }

    return sum / (len - 1);
}
